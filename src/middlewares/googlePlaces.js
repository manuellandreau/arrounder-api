const request = require('request');
const config = require('../config');

// Google places middleware and abstractions
module.exports = {
    /**
     * Get nearby places
     * @param coords // {lagitude, longitude}
     * @param next
     */
    getNearbyPlaces: (coords, next) => {
        const options = {
            method: 'GET',
            url: 'https://maps.googleapis.com/maps/api/place/nearbysearch/json',
            qs: {
                location: coords.latitude + ',' + coords.longitude,
                radius: '200',
                key: 'AIzaSyA_gPCjxT9J-VRn0-gO3uv5-gq2ewXR9Q0'
            }
        };

        request(options, (error, response, body) => {
            if (error) throw new Error(error);

            const places = JSON.parse(body).results.map(place => {
                return {placeId: place.id, placeName : place.name}
            });

            next(places);
        });
    }
};